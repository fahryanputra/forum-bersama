<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
    protected $guarded=[];

    public function answers() {
        return $this->hasMany('App\Answers');
    }
}
