<?php

namespace App\Http\Controllers;

use App\Profiles;
use Illuminate\Http\Request;
use Auth;
use DB;
class ProfileController extends Controller
{
    //
    public function __construct(){
        $this->middleware('auth');
    }
    public function create(){
        return view('profiles.create');
    }
    public function store(Request $request){
        // dd(Auth::id());
        // dd($request->all());
        $request->validate([
            'fullName'=>'required',
            'description'=>'required'
        ]);

        $profile = Profiles::create([
            'full_name'=>$request['fullName'],
            'description'=>$request['description'],
            'user_id'=>Auth::id()
        ]);

        // $profile = DB::table('profiles')->insert([
        //     'full_name'=>$request['fullName'],
        //     'description'=>$request['description'],
        //     'user_id'=>Auth::id()
        // ]);

        return redirect('/questions');
    }
    public function index(){
        // $questions = DB::table('pertanyaan')->get();
        $profile = Profiles::find(Auth::id());
        // dd($profile);
        return view('profiles.index', compact('profile'));
    }
    public function edit($profil_id){
        $profile=Profiles::find($profil_id);
        return view('profiles.edit',compact('profile'));
    }
    public function update(Request $request){
        // dd($request['body']);
        $request->validate([
            'body'=>'required'
        ]);
        $profile = Profiles::where('id',Auth::id())->update([
            'description'=> $request['body']
        ]);
        return redirect('/profiles');
    }
}
