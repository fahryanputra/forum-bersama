<?php

namespace App\Http\Controllers;

use App\Answers;
use Illuminate\Http\Request;
use DB;

class AnswersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

   public function index()
   {
     $answers = Answers::all();

     return view('answers.index', compact('answers'));
   }

   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {
       return view('answers.create');
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {
       //validate
       $this->validate($request, [

         'body'=>'required'
       ]);

       //store
       Answers::create([
         'body' => $request['body'],
         'author' => auth()->user()->name,
         'user_id' => auth()->user()->id
       ]);

       //redirect
       return redirect('/answers');
   }

   /**
    * Display the specified resource.
    *
    * @param  \App\Answers  $questions
    * @return \Illuminate\Http\Response
    */
   public function show($id)
   {
       $answer = Answers::find($id);

       return view('answers.show', compact('answer'));
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Answers  $questions
    * @return \Illuminate\Http\Response
    */
   public function edit($id)
   {
       $answer = Answers::find($id);

       return view('answers.edit', compact('answer'));
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Answers  $questions
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request, $id)
   {
       Answers::where('id', $id)->update([
         'body' => $request['body']
       ]);

       return redirect('/answers');
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Answers  $questions
    * @return \Illuminate\Http\Response
    */
   public function destroy($id)
   {
       Answers::destroy($id);

       return redirect('/answers');
   }
}
