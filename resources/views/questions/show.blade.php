@extends('layouts.app')

@section('heading', 'Tampilkan Pertanyaan')

@section('content')
  <h3>{{ $question->title }}</h3>
  <p style="font-size: 18px">{{ $question->body }}</p>
  <p style="font-size: 15px">by {{$question->author}} at {{ $question->created_at }}</p>
  <a href="{{ route('answers.create') }}" class="btn btn-default">Jawab</a>
  <div class="row mt-4">
    <div class="col-sm-12 text-right">
      <a href="{{ url()->previous() }}" class="btn btn-default">Kembali</a>
      <a href="{{ route('questions.edit', ['question' => $question->id]) }}" class="btn btn-primary">Edit</a>
    </div>
    <div class="mt-4 col-sm-12 text-right">
      <form action="{{ route('questions.destroy', ['question' => $question->id]) }}" method="post">
        @csrf
        @method('DELETE')
        <input type="submit" name="delete" value="Delete" class="btn btn-danger">
      </form>
    </div>
  </div>
@endsection
