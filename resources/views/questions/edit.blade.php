@extends('layouts.app')

@section('heading', 'Edit Pertanyaan')

@section('content')
  <form class="" action="{{ route('questions.update', ['question' => $question->id]) }}" method="post">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label class="col-form-label col-form-label-lg" for="inputLarge">Judul</label>
      <input class="form-control form-control-lg" name="title" type="text" placeholder="Tulis Judul" value="{{ $question->title }}" id="inputLarge">
    </div>
    <div class="form-group">
      <label for="exampleTextarea">Isi Pertanyaan</label>
      <textarea class="form-control" name="body" id="exampleTextarea" placeholder="Tulis Pertanyaan" rows="3">{{ $question->body }}</textarea>
    </div>
    <div class="row mt-4">
      <div class="col-sm-12 text-right">
        <a href="{{ url()->previous() }}" class="btn btn-default">Kembali</a>
        <button type="submit" class="btn btn-primary">Edit</button>
      </div>
    </div>
  </form>
@endsection
