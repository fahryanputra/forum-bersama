@extends('layouts.app')

@section('heading', 'Lengkapi Data Diri')

@section('content')
    <h5>Lengkapi data diri terlebih dahulu agar kami dapat lebih mengenal anda</h5>
  <form class="" action="/profiles" method="post">
    @csrf
    <div class="form-group">
      <label class="col-form-label" for="fullName">Nama Lengkap</label>
      <input class="form-control" name="fullName" type="text" placeholder="Tulis Nama Lengkap" id="fullName">
    </div>
    <div class="form-group">
      <label for="description">Deskripsikan Dirimu</label>
      <textarea class="form-control" name="description" id="description" placeholder="Tulis Deskripsimu" rows="3"></textarea>
    </div>
    <div class="row mt-4">
      <div class="col-sm-12 text-right">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </form>
@endsection
