@extends('layouts.app')

@section('content')

<form class="" action="/profiles" method="post">
    @csrf
    @method('PUT')
      <label for="exampleTextarea">Deskripsi</label>
      <textarea class="form-control" name="body" id="exampleTextarea" placeholder="Tulis Deskripsi" rows="3"></textarea>
    </div>
    <div class="row mt-4">
      <div class="col-sm-12 text-right">
        <a href="{{ url()->previous() }}" class="btn btn-default">Kembali</a>
        <button type="submit" class="btn btn-primary">Edit</button>
      </div>
    </div>
  </form>
  @endsection