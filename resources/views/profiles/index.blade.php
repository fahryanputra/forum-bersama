@extends('layouts.app')

@section('heading', 'Profile')
@section('button')
    <a href="profiles/{{$profile->id}}/edit" class="btn btn-primary">Edit Description</a>
@endsection
@section('content')
    <h2>Hello, {{ $profile->full_name }}</h2>
    <br>
    <h4>Description:</h4>
    <h5>{{ $profile->description }}</h5>
@endsection
