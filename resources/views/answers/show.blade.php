@extends('layouts.app')

@section('heading', 'Tampilkan Jawaban')

@section('content')
  <p style="font-size: 18px">{{ $answer->body }}</p>
  <p style="font-size: 15px">by {{$answer->author}} at {{ $answer->created_at }}</p>
  <div class="row mt-4">
    <div class="col-sm-12 text-right">
      <a href="{{ url()->previous() }}" class="btn btn-default">Kembali</a>
      <a href="{{ route('answers.edit', ['answer' => $answer->id]) }}" class="btn btn-primary">Edit</a>
    </div>
    <div class="mt-4 col-sm-12 text-right">
      <form action="{{ route('answers.destroy', ['answer' => $answer->id]) }}" method="post">
        @csrf
        @method('DELETE')
        <input type="submit" name="delete" value="Delete" class="btn btn-danger">
      </form>
    </div>
  </div>
@endsection