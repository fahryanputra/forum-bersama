@extends('layouts.app')

@section('heading', 'Edit Jawaban')

@section('content')
  <form class="" action="{{ route('answers.update', ['answer' => $answer->id]) }}" method="post">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label for="exampleTextarea">Isi Jawaban</label>
      <textarea class="form-control" name="body" id="exampleTextarea" placeholder="Tulis Jawaban" rows="3">{{ $answer->body }}</textarea>
    </div>
    <div class="row mt-4">
      <div class="col-sm-12 text-right">
        <a href="{{ url()->previous() }}" class="btn btn-default">Kembali</a>
        <button type="submit" class="btn btn-primary">Edit</button>
      </div>
    </div>
  </form>
@endsection
