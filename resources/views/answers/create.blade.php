@extends('layouts.app')

@section('heading', 'Buat Jawaban')

@section('content')
  <form class="" action="{{ route('answers.store') }}" method="post">
    @csrf
    <div class="form-group">
      <label for="exampleTextarea">Isi Jawaban</label>
      <textarea class="form-control" name="body" id="exampleTextarea" placeholder="Tulis Pertanyaan" rows="3">{{ old('body') }}</textarea>
    </div>
    <div class="row mt-4">
      <div class="col-sm-12 text-right">
        <a href="{{ url()->previous() }}" class="btn btn-default">Kembali</a>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </form>
@endsection
