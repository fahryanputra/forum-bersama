@extends('layouts.app')

@section('heading', 'Jawaban')

@section('button')
  <div class="col-sm-2 my-auto">
    <a class="btn btn-primary" href="{{ route('answers.create') }}">Menjawab</a>
  </div>
@endsection

@section('content')
  <div class="list-group">
    @forelse ($answers ?? '' as $key => $answer)
      <a href="{{ route('answers.show', ['answer' => $answer->id]) }}" class="list-group-item list-group-item-action flex-column align-items-start">
        <div class="d-flex w-100 justify-content-between">
          <small class="text-muted">{{ $answer->created_at }}</small>
        </div>
        <p class="mb-1">{{ $answer->body }}</p>
        <small class="text-muted">{{ $answer->author_id }}</small>
      </a>
    @empty
      <h5>No Data</h5>
    @endforelse
  </div>
@endsection
